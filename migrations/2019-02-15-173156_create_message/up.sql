CREATE TABLE users (
  id SERIAL NOT NULL,
  name TEXT NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO users (name) VALUES ('John');
INSERT INTO users (name) VALUES ('Jane');

CREATE TABLE messages (
    id SERIAL NOT NULL,
    content TEXT NOT NULL,
    created timestamptz not null DEFAULT NOW(),
    user_id INT NOT NULL REFERENCES users,
    PRIMARY KEY (id)
);
