extern crate diesel_demo;
extern crate diesel;

use self::diesel_demo::*;
use self::models::*;
use self::diesel::prelude::*;

fn main() {
    use diesel_demo::schema::messages::dsl::*;

    let connection = establish_connection();
    let results = messages
        .limit(5)
        .load::<Message>(&connection)
        .expect("Error loading messages");

    println!("Displayng {} messages", results.len());
    for msg in results {
        println!("P: {}", msg.content);
    }
}
