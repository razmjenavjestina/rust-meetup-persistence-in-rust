#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate diesel;
extern crate diesel_codegen;
extern crate uuid;
extern crate postgres;

mod controller;

fn main() {
    rocket::ignite()
        .mount(
            "/",
            routes![controller::create_message,
                    controller::get_messages,
                    controller::get_message,
                    controller::get_messages_bare,
                    controller::update_message,

                    controller::get_users,
                    controller::get_user_messages,
            ],
        )
        .launch();
}
