use diesel_demo::models::Message;
use diesel_demo::models::User;
use diesel_demo::models::*;
use rocket_contrib::json::Json;

#[get("/users")]
pub fn get_users() -> Json<Vec<User>> {
    use diesel::query_dsl::RunQueryDsl;
    use diesel_demo::schema::users::dsl::*;

    let connection = diesel_demo::establish_connection();
    let results = users
        .load::<User>(&connection)
        .expect("Error loading users");
    Json(results)
}


#[get("/messages")]
pub fn get_messages() -> Json<Vec<Message>> {
    use diesel::query_dsl::RunQueryDsl;
    use diesel::query_dsl::QueryDsl;
    use diesel::TextExpressionMethods;
    use diesel_demo::schema::messages::dsl::*;

    let connection = diesel_demo::establish_connection();
    let results = messages
        .filter(content.like("%ust%"))
        .load::<Message>(&connection)
        .expect("Error loading messages");
    Json(results)
}










#[get("/messages/<msg_id>")]
pub fn get_message(msg_id: i32) -> Json<Message> {
    use diesel::query_dsl::RunQueryDsl;
    use diesel::query_dsl::QueryDsl;
    use diesel_demo::schema::messages::dsl::*;

    let connection = diesel_demo::establish_connection();
    let results = messages.find(msg_id).get_result(&connection)
        .expect(&format!("Error loading message with id {}", msg_id));
    Json(results)
}










#[post("/messages", data = "<msg_body>")]
pub fn create_message(msg_body: Json<CreateMessage>) -> Json<Message> {
    use diesel::query_dsl::RunQueryDsl;
    let connection = diesel_demo::establish_connection();

    let msg = msg_body.into_inner();

    let new_msg = CreateMessage {
        content: msg.content,
        user_id: msg.user_id
    };

    Json(
        diesel::insert_into(diesel_demo::schema::messages::table)
            .values(new_msg)
            .get_result(&connection)
            .expect("Error saving new post"),
    )
}












#[get("/messages/bare")]
pub fn get_messages_bare() -> Json<Vec<Message>> {
    use postgres::{Connection, TlsMode};
    use dotenv::dotenv;
    use std::env;
    dotenv().ok();

    let conn = Connection::connect(
        env::var("DATABASE_URL").expect("Database url must be set"),
        TlsMode::None).unwrap();

    let query_result = &conn.query("SELECT * FROM messages", &[]).unwrap();
    let mut res = Vec::with_capacity(query_result.len());
    for row in query_result {
        let obj: Message = ORM::from_db(&row);
        //let obj = Message {
        //    id: row.get(0),
        //    content: row.get(1),
        //    created: row.get(2),
        //    user_id: row.get(3)
        //};
        res.push(obj);
    }
    Json(res)
}







#[put("/messages/<msg_id>", data = "<msg_body>")]
pub fn update_message(msg_id: i32, msg_body: Json<CreateMessage>) -> Json<Message> {
    let connection = diesel_demo::establish_connection();
    use diesel::query_dsl::QueryDsl;
    use diesel::query_dsl::RunQueryDsl;
    use diesel_demo::schema::messages::dsl::*;

    let q = messages.find(msg_id);
    let msg: Message = q.first(&connection).expect("Message not found");
    let r = diesel::update(&msg)
        .set(msg_body.into_inner())
        .get_result(&connection)
        .expect("update failed");
    Json(r)
}















#[get("/users/<user_id>/messages")]
pub fn get_user_messages(user_id: i32) -> Json<Vec<Message>> {
    use diesel::query_dsl::QueryDsl;
    use diesel::query_dsl::RunQueryDsl;
    use diesel::BelongingToDsl;
    use diesel_demo::schema;

    let connection = diesel_demo::establish_connection();
    let usr = schema::users::dsl::users.find(user_id)
        .get_result::<User>(&connection)
        .expect("User not found");

    let msgs = Message::belonging_to(&usr)
        .get_results(&connection)
        .expect("Messages not found");

    Json(msgs)
}

