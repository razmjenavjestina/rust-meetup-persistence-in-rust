use chrono::{DateTime, Utc};

use super::schema::messages;
use super::schema::users;
use diesel;
use serde_derive::*;

#[derive(Queryable, Debug, Serialize, Identifiable)]
#[table_name = "users"]
pub struct User {
    pub id: i32,
    pub name: String,
}

#[derive(Queryable, Debug, Serialize, Identifiable, Associations)]
#[belongs_to(User, foreign_key="user_id")]
pub struct Message {
    pub id: i32,
    pub content: String,
    pub created: DateTime<Utc>,
    pub user_id: i32,
}

#[derive(Deserialize, Serialize, Insertable, AsChangeset)]
#[table_name = "messages"]
pub struct CreateMessage<'a> {
    pub content: &'a str,
    pub user_id: i32,
}








pub trait ORM {
    fn from_db(row: &postgres::rows::Row) -> Self;
}

impl ORM for Message {
    fn from_db(row: &postgres::rows::Row ) -> Message {
        Message {
            id: row.get(0),
            content: row.get(1),
            created: row.get(2),
            user_id: row.get(3)
        }
    }
}
