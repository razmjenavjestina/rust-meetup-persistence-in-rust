table! {
    messages (id) {
        id -> Int4,
        content -> Text,
        created -> Timestamptz,
        user_id -> Int4,
    }
}

table! {
    users (id) {
        id -> Int4,
        name -> Text,
    }
}

joinable!(messages -> users (user_id));

allow_tables_to_appear_in_same_query!(
    messages,
    users,
);
